package anticheCiviltÓ;

public class RisorsaMancanteException extends RuntimeException {
	public RisorsaMancanteException(String msg) {
		super(msg);
	}
	
	public RisorsaMancanteException() {
		super();
	}

}
