package anticheCivilt�;

import java.util.ArrayList;
import java.util.Random;

/**
 * Classe che rappresenta una risorsa con il suo prezzo
 * @author misino
 *
 */
public class Risorsa implements Comparable<Risorsa> {
	//CAMPI
	private String nome;
	private int prezzo;
	private static ArrayList<Risorsa> risorseCreate = new ArrayList<Risorsa>();
	
	
	//COSTRUTTORI
	/**
	 * Costruttore della classe Risorsa, si occupa anche dell'aggiunga alla lista statica <code>risorseCreate</code>
	 * @param nome Nome della risorsa
	 * @param prezzo intero che rappresenta il prezzo
	 */
	public Risorsa(String nome,int prezzo) {
		this.nome = nome;
		this.prezzo = prezzo;
		risorseCreate.add(this);
	}
	
	//METODI
	/**
	 * Stampa la risorsa nella forma nomeRisorsa (prezzo)
	 * @return la stringa nomeRisorsa (prezzo)
	 */
	public String toString() {
		return nome + " (prezzo "+prezzo+")";
	}
	
	

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + prezzo;
		return result;
	}

	/** 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Risorsa))
			return false;
		Risorsa other = (Risorsa) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (prezzo != other.prezzo)
			return false;
		return true;
	}
	
	/**
	 * Implementazione dell'interfaccia comparable con compareTo. Un oggetto � minore se il prezzo � minore, se il prezzo � uguale si
	 * confrontanto in modo lessicografico i nomi
	 * @return restituisce un intero tra -1,0,1 a seconda che l'oggetto sia rispettivamente minore, uguale o maggiore dell'argomento
	 */
	public int compareTo(Risorsa other){
		if(this.prezzo>other.prezzo)
			return 1;
		else if(this.prezzo<other.prezzo)
			return -1;
		else if(nome.compareTo(other.nome)<0)
			return -1;
		else if(nome.compareTo(other.nome)>0)
			return 1;
		return 0;		
	}
	
	/**
	 * Restituisce una Risorsa casuale tra quelle presenti in risorseCreate
	 * @return risorsa casuale tra quelle create.
	 */
	public static Risorsa risorsaCasuale() {
		Random rand = new Random();
		int temp = rand.nextInt(risorseCreate.size());
		return risorseCreate.get(temp);
		}

	/**
	 * @return the prezzo
	 */
	public int getPrezzo() {
		return prezzo;
	}
	
	public String getNome() {
		return nome;
	}
	


}
