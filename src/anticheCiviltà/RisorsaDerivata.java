package anticheCiviltÓ;

public class RisorsaDerivata extends Risorsa {
	private Risorsa originale;
	
	public RisorsaDerivata(String nome, int prezzo, Risorsa originale) {
		super(nome,prezzo);
		this.originale = originale;
	}
	
	public Risorsa getOriginale(){
		return originale;
	}

}
