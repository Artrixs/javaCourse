package anticheCivilt�;

/**
 * Rappresenta una citta di tipo Industriale, che produce una certa risorsa
 * @author misino
 *
 */
public class CittaIndustriale extends Citta {
	private Risorsa risorsa;
	
	/**
	 * Crea una citt� che produce un certo tipo di risorsa
	 * @param nome il nome della citt�
	 * @param risorsa il tipo di risosrsa che la citt� produce
	 */
	public CittaIndustriale(String nome,Risorsa risorsa) {
		super(nome);
		this.risorsa = risorsa;
	}

	@Override
	public void produce(Civilta c) {
		if(risorsa instanceof RisorsaDerivata) {
			RisorsaDerivata newRisorsa = (RisorsaDerivata) risorsa;
			if(c.possiede(newRisorsa.getOriginale()))
				c.aggiugniRisorsa(newRisorsa);
			else
				throw new RisorsaMancanteException("La risorsa originale non � disponible");
		}else
			c.aggiugniRisorsa(risorsa);
	}
	
	@Override
	public String toString(){
		return this.getNome()+ ", una citt� di tipo Industriale, che controlla la risorsa "+risorsa.toString();
	}
	
	public Risorsa getRisorsa() {
		return risorsa;
	}


}
