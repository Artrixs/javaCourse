package anticheCivilt�;

import java.util.ArrayList;
/**
 * Classe che rappresenta una civilta e le sue possibli azioni
 * @author misino
 *
 */
public class Civilta {
	//CAMPI
	private String nome;
	private int tesoro;
	private ArrayList<Risorsa> stock;
	private ArrayList<Citta> citta;
	
	//COSTRUTTORI
	/**
	 * Crea una civilt� con tesoro = 0 e nessuna citta o risorsa
	 * @param nome il nome della citt�
	 */
	public Civilta(String nome) {
		this.nome = nome;
		tesoro = 0;
		stock = new ArrayList<Risorsa>();
		citta = new ArrayList<Citta>();
	}
	
	//METODI
	/**
	 * fonda una nuova citta e l'aggiunge all'elenco delle citt� della civilt�
	 * @param nome il nome della citt�
	 * @param tipo 'e' se economica 'i' se industriale
	 * @return la citta appena creata
	 */
	public Citta fondaCitta(String nome, char tipo) {
		Citta city = null;
		if(tipo == 'e') 
			city = new CittaEconomica(nome);		
		if(tipo == 'i') 
			city = new CittaIndustriale(nome,Risorsa.risorsaCasuale());
		citta.add(city);
		return city;		
	}

	/**
	 * aggiunge la risorsa indicata allo stock
	 * @param risorsa
	 */
	public void aggiugniRisorsa(Risorsa risorsa) {
		stock.add(risorsa);
		
	}

	/**
	 * aggiunge la quantita di denara indicata al tesoro
	 * @param d
	 */
	public void aggiungiDenaro(int d) {
		tesoro += d;		
	}
	
	/**
	 * Richiama il metodo produce su tutte le citt� appartenteni a questa civilt�
	 */
	public void faiProdurre() {
		for(Citta city : citta)
			try {
				city.produce(this);
			} catch (RisorsaMancanteException e) {
				CittaIndustriale newCity = (CittaIndustriale) city;
				System.out.println("La citt� "+city.getNome()+" non pu� produrre la risorsa derivata \""+newCity.getRisorsa()+"\" perch� non � disponibile la risorsa originale \""+((RisorsaDerivata) newCity.getRisorsa()).getOriginale()+"\".");
			}
	}
	
	/**
	 * Effetua la vendita verso un altra civilt� come specificato nel contratto
	 * @param altra l'altra civilta verso cui si commercia
	 * @return booleano se � avvenuto almeno un commercio.
	 */
	public boolean vendiRisorseA(Civilta altra) {
		boolean result = false;
		ArrayList<Risorsa> nuovoStock = new ArrayList<Risorsa>();
		for(Risorsa ris:stock) {
			if(possiedeDoppia(ris))
				if(!altra.possiede(ris)) {
					altra.aggiugniRisorsa(ris);
					altra.aggiungiDenaro(-ris.getPrezzo());
					tesoro += ris.getPrezzo();
					result = true;
					System.out.println(this.toString()+" ha venduto "+ris.toString()+" a "+altra.toString());
					break;
				}
			nuovoStock.add(ris);
		}
		stock = nuovoStock;
		return result;
	}

	public boolean possiede(Risorsa risorsa) {
		return stock.contains(risorsa);
	}
	
	private boolean possiedeDoppia(Risorsa risorsa) {
	if(!possiede(risorsa))
		return false;
	else if(stock.indexOf(risorsa) == stock.lastIndexOf(risorsa))
		return false;
	else 
		return true;
	}
	
	public String toString() {
		return nome +" ($"+tesoro+")";
	}
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @return the tesoro
	 */
	public int getTesoro() {
		return tesoro;
	}

	public static void main(String[] args) {
		Risorsa ris1 = new Risorsa("carne",50);
		Risorsa ris2 = new RisorsaDerivata("prosciutto",75,ris1);
		Civilta italia = new Civilta("Italia");
		italia.fondaCitta("milano", 'i');
		italia.fondaCitta("roma", 'i');
		italia.fondaCitta("torino", 'i');
		
		Civilta francia = new Civilta("Francia");
		francia.fondaCitta("parigi", 'e');
		
		italia.faiProdurre();
		italia.faiProdurre();
		francia.faiProdurre();
		System.out.println(francia.vendiRisorseA(italia));
		System.out.println(italia.vendiRisorseA(francia));
		System.out.println("FATTO");
	}
}
