package anticheCiviltÓ;

public abstract class Citta {
	
	private String nome;
	
	public Citta(String nome) {
		this.nome = nome;
	}
	
	public abstract void produce(Civilta c);
	public String toString() {
		return nome;
	}
	
	public String getNome(){
		return nome;
	}

}
