package anticheCivilt�;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Classe di test per che racchiude una serie di civilt�, e le fa evolvere
 * @author misino
 *
 */
public class Storia {
	//CAMPI
	private Civilta[] civilta;
	
	
	//COSTRUTTORI
	/**
	 * Instanzia la classe partendo da un array di civilt�
	 * @param civilta array di civilt�
	 */
	public Storia(Civilta[] civilta) {
		this.civilta = civilta;
	}
	
	
	//METODI
	public Civilta commercia(int n) {
		
		Civilta maxCiv = null;
		int maxTesoro = 0;
		
		for(int i=0;i<n;i++) {
			System.out.println("Ciclo #"+(i+1));
			for(Civilta civ:civilta) //Chiama il metodo faiProdurre su ogni civilta
				civ.faiProdurre();
			
			for(Civilta civ:civilta) //Prova a vendere con tutte le altre civilt�
				for(Civilta altra:civilta) {
					if(civ!=altra) {
						if(civ.vendiRisorseA(altra))//se vende risorse si interrompe, e passa alla civilt� successiva
							break;
					}
				}
			
			System.out.println(toString()); //Stampa il riepilogo deli tesori
		
		}
		for(Civilta civ:civilta) {//Calcola la civilt� con il tesoro pi� grande.
			if(civ.getTesoro()>maxTesoro) {
				maxTesoro = civ.getTesoro();
				maxCiv = civ;
			}
		}	
		
		return maxCiv;
		
	}
	
	/**
	 * Stampa il riepilogo delle civilta e dei loro tesori.
	 */
	public String toString() {
		String result = "";
		for(Civilta civ:civilta) {
			result += civ.getNome() + " possiede "+civ.getTesoro() + " $. \n ";
		}
		return result;
	}

	
	
	//METODI STATICI 
	public static void main(String[] args) {
		//CREA RISORSE
		Scanner sc = new Scanner(System.in);
		System.out.println("Inserisci il nome di una nuova risorsa");
		String nome = sc.nextLine();
		int prezzo;
		Risorsa tempRisorsa;
		while(!nome.equals("stop")) {
			System.out.println("Qual'� il suo prezzo?");
			prezzo = new Integer(sc.nextLine());
			tempRisorsa = new Risorsa(nome,prezzo);
			
			//Mi occupo di un eventuale risosrsa derivata
			System.out.println("Vuoi creare una risorsa derivata da "+tempRisorsa.toString()+"? [y/n]");
			if(sc.nextLine().equals("y")) {
				System.out.println("Qual'� il suon nome?");
				nome = sc.nextLine();
				System.out.println("Qual'� il suo prezzo?");
				prezzo = new Integer(sc.nextLine());
				new RisorsaDerivata(nome,prezzo,tempRisorsa);
			}
			
			System.out.println("Inserisci il nome di una nuova risorsa, o \"stop\" per non inserire altre risorse.");
			nome = sc.nextLine();
		}

		//CREA CIVILTA
		ArrayList<Civilta> civilta = new ArrayList<Civilta>();
		System.out.println("Inserisci il nome di una civilt�");
		String nomeCiv = sc.nextLine();
		while(!nomeCiv.equals("")) {
			Civilta tempCiv = new Civilta(nomeCiv);
			System.out.println("Inserisci il nome di una citt� o stop per non inserire altre citt�");
			String nomeCitta = sc.nextLine();
			while(!nomeCitta.equals("stop")) {
				System.out.println("Di che tipo deve essere?");
				if(sc.nextLine().equals("i"))
					System.out.println(nomeCiv+" ha fondato "+tempCiv.fondaCitta(nomeCitta, 'i'));
				else
					System.out.println(nomeCiv+" ha fondato "+tempCiv.fondaCitta(nomeCitta, 'e'));
				
				System.out.println("Inserisci il nome di una citt� o stop per non inserire altre citt�");
				nomeCitta = sc.nextLine();
			}
			
			civilta.add(tempCiv);
			System.out.println("Inserisci il nome di una civilt� o una stringa vuota per terminare.");
			nomeCiv = sc.nextLine();
		}
			
		Civilta[] temp = new Civilta[civilta.size()];
		temp = civilta.toArray(temp);

		
		//INIZIO DELLA STORIA
		Storia storia = new Storia(temp);
		System.out.println("Quanti cicli vuoi eseguire?");
		int cicli = new Integer(sc.nextLine());
		System.out.println("La civilt� pi� ricca � "+storia.commercia(cicli));

		
		sc.close();

	}
	
	/**
	 * Subroutine che si occupa di creare le risorse.
	 */
	private static void creaRisorse() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Inserisci il nome di una nuova risorsa");
		String nome = sc.nextLine();
		int prezzo;
		Risorsa tempRisorsa;
		while(!nome.equals("stop")) {
			System.out.println("Qual'� il suo prezzo?");
			prezzo = new Integer(sc.nextLine());
			tempRisorsa = new Risorsa(nome,prezzo);
			
			//Mi occupo di un eventuale risosrsa derivata
			System.out.println("Vuoi creare una risorsa derivata da "+tempRisorsa.toString()+"? [y/n]");
			if(sc.nextLine().equals("y")) {
				System.out.println("Qual'� il suon nome?");
				nome = sc.nextLine();
				System.out.println("Qual'� il suo prezzo?");
				prezzo = new Integer(sc.nextLine());
				new RisorsaDerivata(nome,prezzo,tempRisorsa);
			}
			
			System.out.println("Inserisci il nome di una nuova risorsa, o \"stop\" per non inserire altre risorse.");
			nome = sc.nextLine();
		}
		
		

	}
	
	/**
	 * Subroutine per la creazione di una civilt�.
	 */
	private static Civilta[] creaCivilta(){
		ArrayList<Civilta> civilta = new ArrayList<Civilta>();
		Scanner sc = new Scanner(System.in);
		System.out.println("Inserisci il nome di una civilt�");
		String nomeCiv = sc.nextLine();
		while(!nomeCiv.equals("")) {
			Civilta tempCiv = new Civilta(nomeCiv);
			System.out.println("Inserisci il nome di una citt� o stop per non inserire altre citt�");
			String nomeCitta = sc.nextLine();
			while(!nomeCitta.equals("stop")) {
				System.out.println("Di che tipo deve essere?");
				if(sc.nextLine().equals("i"))
					System.out.println(nomeCiv+" ha fondato "+tempCiv.fondaCitta(nomeCitta, 'i'));
				else
					System.out.println(nomeCiv+" ha fondato "+tempCiv.fondaCitta(nomeCitta, 'e'));
				
				System.out.println("Inserisci il nome di una citt� o stop per non inserire altre citt�");
				nomeCitta = sc.nextLine();
			}
			
			System.out.println("Inserisci il nome di una civilt� o una stringa vuota per terminare.");
			nomeCiv = sc.nextLine();
		}
		
		
		
		
		sc.close();
		Civilta[] temp = new Civilta[civilta.size()];
		temp = civilta.toArray(temp);
		return temp;
	}

}
