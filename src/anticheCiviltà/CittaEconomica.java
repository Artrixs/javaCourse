package anticheCivilt�;

/**
 * Crea una classe che rappresenta una citt� di tipo Economico, ovvero che produce denaro
 * @author misino
 *
 */
public class CittaEconomica extends Citta {

	/**
	 * Crea la citt�Economica
	 * @param nome Il nome della citta economica
	 */
	public CittaEconomica(String nome) {
		super(nome);
	}
	
	@Override
	public void produce(Civilta c) {
		c.aggiungiDenaro(1000);
	}
	
	public String toString() {
		return this.getNome() + ", una citt� di tipo Economico";
	}

}
