package supermercato;

public class ScontoSemplice implements Promozione{
	
	private double sconto;

	public ScontoSemplice(double sconto) {
		if(sconto<=1&&sconto>0)
			this.sconto = sconto;
		else if(sconto>0&&sconto<=100)
			this.sconto = sconto/100;
		else this.sconto = 0;
	}
	
	public double prezzoScontato(int quantita,double prezzo){
		return quantita*prezzo-quantita*prezzo*sconto;
	}

}
