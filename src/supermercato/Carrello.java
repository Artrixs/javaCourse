package supermercato;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class Carrello {
	
	private GregorianCalendar data;
	private boolean fidelity;
	private ArrayList<Prodotto> prodotti;
	private ArrayList<Integer> quantita;
	
	//COSTRUTTORI
	public Carrello(GregorianCalendar data, boolean fidelity) {
		this.data = data;
		this.fidelity = fidelity;
		this.prodotti = new ArrayList<Prodotto>();
		this.quantita = new ArrayList<Integer>();
	}
	
	
	//METODI
	public boolean getFidelity() {
		return fidelity;
	}
	
	public GregorianCalendar getData() {
		return data;
	}

	public void add(Prodotto p,int q) {
		if(prodotti.contains(p)) {
			int pos = prodotti.indexOf(p);
			int temp = quantita.get(pos);
			quantita.remove(pos);
			quantita.add(pos, temp + q);
		}else {
			prodotti.add(p);
			quantita.add(q);
		}
	}
	
	public int size() {
		return prodotti.size();
	}
	
	public Prodotto get(int i) throws ArrayIndexOutOfBoundsException {
		if(i>=0&&i<prodotti.size())
			return prodotti.get(i);
		else
			throw new ArrayIndexOutOfBoundsException();
	}
	
	public int quantity(int i) throws ArrayIndexOutOfBoundsException {
		if(i>=0&&i<quantita.size())
			return quantita.get(i);
		else
			throw new ArrayIndexOutOfBoundsException();
	}
	
	public String toString() {
		String result = "Il carrello contiene " + size() + "prodotti diversi\n";
		for(Prodotto p:prodotti) {
			result += quantity(prodotti.indexOf(p)) +" "+p.toString()+"\n";
		}
		return result;
	}

	public static void main(String[] args) {
		Prodotto fagioliBounduelle, zucchini, cetrioli;
		CategoriaMerceologica verdura,verduraFresca,verduraScatola;
		
		verdura = new CategoriaMerceologica("Verdura","verdure");	
		verduraFresca = new CategoriaMerceologica("Verdura Fresca","verdure",verdura);
		verduraScatola = new CategoriaMerceologica("Verdura in scatola","verdure",verdura);
		
		fagioliBounduelle = new Prodotto("Fagioli","Bonduelle",verduraScatola,"ABC67437",1.45);
		zucchini = new Prodotto("Zucchine","-",verduraFresca,"ABs67437",2);
		cetrioli = new Prodotto("Cetrioli","-", verduraFresca,"cdss45",1.5);
		
		CampagnaPromozionale campagna = new CampagnaPromozionale(new GregorianCalendar(2018,0,12,16,8), new GregorianCalendar(2018,0,15,0,0),true);
		Promozione promo = new ScontoSemplice(50);
		campagna.add(zucchini, promo);
		
		try {
			campagna.setPuntiFedelta(1, 10);
		} catch (UnsupportedOperationException e) {
			System.out.println("La campagna non � riservata!");
		}
		
		Carrello carrello = new Carrello(new GregorianCalendar(),true);
		carrello.add(fagioliBounduelle, 5);
		carrello.add(zucchini, 4);
		carrello.add(cetrioli, 3);
		
		System.out.println(carrello.toString());
		carrello.add(cetrioli, 3);
		System.out.println(carrello.toString());


	}

}
