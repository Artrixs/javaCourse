package supermercato;

public class TrePerDue extends KPerH implements Promozione {

	public TrePerDue() {
		super(3, 2);
	}
	
	public static void main(String[] args) {
		KPerH sconto = new TrePerDue();
		System.out.println(sconto.prezzoScontato(4, 1.3));
	}

}
