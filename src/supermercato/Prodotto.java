package supermercato;

public class Prodotto {
	//CAMPI
	private String nome, produttore, codice;
	private CategoriaMerceologica cat;
	private double prezzo;
	
	
	//COSTRUTTORI
	public Prodotto(String nome, String produttore, CategoriaMerceologica cat, String codice, double prezzo){
		this.nome = nome;
		this.produttore = produttore;
		this.cat = cat;
		this.codice = codice;
		this.prezzo = prezzo;
	}
	
	//METODI
	public String toString(){
		return nome +" ("+cat.toString()+") prodotto da " + produttore +" ("+prezzo+"�)"; 
	}
	
	public boolean stessaCategoriaDi(Prodotto prod){
		return this.cat == prod.cat;
	}
	
	public boolean stessaFamigliaDi(Prodotto prod){
		return this.cat.radice() == prod.cat.radice();
	}
	
	public double getPrezzo(){
		return prezzo;
	}


	public static void main(String[] args) {
			
		

	}

}
