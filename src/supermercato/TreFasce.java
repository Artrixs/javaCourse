package supermercato;

public class TreFasce implements Promozione {
	
	private int q1,q2;
	private double p1,p2;
	
	public TreFasce(int q1,int q2,double p1,double p2){
		this.q1 = q1;
		this.q2 = q2;
		this.p1 = p1;
		this.p2 = p2;
	}

	@Override
	public double prezzoScontato(int quantita, double prezzo) {
		ScontoSemplice sconto;
		if(quantita<q1)
			return quantita*prezzo;
		else if(quantita<q2)
			sconto = new ScontoSemplice(p1);
		else
			sconto = new ScontoSemplice(p2);
		
		return sconto.prezzoScontato(quantita, prezzo);
	}

}
