package supermercato;

public class CategoriaMerceologica {
	//CAMPI
	private String nome, descrizione;
	private CategoriaMerceologica superCat;
	
	//COSTRUTTORI
	public CategoriaMerceologica(String nome, String descrizione){
		this.nome = nome;
		this.descrizione = descrizione;
		this.superCat = null;	
	}
	
	public CategoriaMerceologica(String nome, String descrizione,CategoriaMerceologica cat){
		this(nome, descrizione);
		this.superCat = cat;
	}
	
	
	//METODI
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return nome +" ("+descrizione+")";
	}
	
	public boolean equals(Object o){
		if(!(o instanceof CategoriaMerceologica))
			return false;
		else{
			CategoriaMerceologica cat = (CategoriaMerceologica) o;
			return this.nome == cat.nome && this.descrizione == cat.descrizione && this.superCat == cat.superCat;
		}
	}
	
	
	/**
	 * restituisce la categoria superiore
	 * @return la CategoriaMerceologica di cui e' sottoclasse o null
	 */
	public CategoriaMerceologica getSuper(){
		return superCat;
	}
	
	public String gerarchia() {
		String result = nome;
		CategoriaMerceologica sup = superCat;
		while(sup!=null){
			result = sup + " / "+result;
			sup = sup.getSuper();
		}
		return result;
	}
	
	public CategoriaMerceologica radice(){
		if(superCat == null)
			return this;
		else
			return superCat.radice();
	}

}
