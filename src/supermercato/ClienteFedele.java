/**
 * Rappresenta un cliente con carta Fedelt� in un supermercato
 * @author Arturo Misino
 * @verison 1.0
 */
package supermercato;

import java.util.GregorianCalendar;

public class ClienteFedele {
	private String nome, cognome, codiceCliente;
	private GregorianCalendar dataNascita;
	
	/**
	 * Costruisce un cliente con i seguenti attribuiti
	 * @param nome
	 * @param cognome
	 * @param codice
	 * @param dataNascita
	 */
	public ClienteFedele(String nome,String cognome,String codice,GregorianCalendar dataNascita) {
		this.nome = nome;
		this.cognome = cognome;
		this.codiceCliente = codice;
		this.dataNascita = dataNascita;
	}

}
