package supermercato;

import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CampagnaPromozionale {
	private GregorianCalendar dataInizio, dataFine;
	private boolean riservata;
	private double puntiFedeltaEuro;
	private int puntiFedelta;
	Map<Prodotto,Promozione> elenco;
	
	//COSTRUTTORI
	public CampagnaPromozionale(GregorianCalendar dataInizio, GregorianCalendar dataFine, boolean riservata ){
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.riservata = riservata;
		this.elenco  = new HashMap<Prodotto,Promozione>();	
		this.puntiFedelta = 0;
		this.puntiFedeltaEuro = 1;
	}
	
	//METODI
	
	public void add(Prodotto p, Promozione prom) throws IllegalArgumentException{
		if(elenco.containsKey(p))
			throw new IllegalArgumentException("Prodotto a cui e' gia' associata una promozione");
		else
			elenco.put(p, prom);
	}
	
	public double prezzo(Prodotto p,int q,GregorianCalendar data, boolean fidelity){
		boolean inCorso = data.after(dataInizio) && data.before(dataFine);
		boolean isFedele;
		if(riservata)
			isFedele = fidelity;
		else
			isFedele = true;
		
		if(inCorso&&isFedele&&elenco.containsKey(p))
			return elenco.get(p).prezzoScontato(q, p.getPrezzo());
		else
			return p.getPrezzo()*q;				
	}
	
	public void setPuntiFedelta(double x, int y) throws UnsupportedOperationException{
		if(!riservata)
			throw new UnsupportedOperationException("La campagna non e' riservata esclusivamente ai fideliy");
		else{
			puntiFedelta = y;
			puntiFedeltaEuro = x;
		}
	}
	
	public int punti(Prodotto p, int q, GregorianCalendar d, boolean fidelity){
		boolean inCorso = d.after(dataInizio) && d.before(dataFine);
		if(inCorso&&elenco.containsKey(p)&&fidelity&&riservata){
			double sconto = p.getPrezzo()*q -elenco.get(p).prezzoScontato(q, p.getPrezzo());
			int punti = ((int) Math.floor(sconto/puntiFedeltaEuro))*puntiFedelta;
			return punti;
		}else
			return 0;
	}

	

	public static void main(String[] args) {
		Prodotto fagioliBounduelle, zucchini;
		CategoriaMerceologica verdura,verduraFresca,verduraScatola;
		
		verdura = new CategoriaMerceologica("Verdura","verdure");	
		verduraFresca = new CategoriaMerceologica("Verdura Fresca","verdure",verdura);
		verduraScatola = new CategoriaMerceologica("Verdura in scatola","verdure",verdura);
		
		fagioliBounduelle = new Prodotto("Fagioli","Bonduelle",verduraScatola,"ABC67437",4);
		zucchini = new Prodotto("Zucchine","-",verduraFresca,"ABs67437",2);
		
		CampagnaPromozionale campagna = new CampagnaPromozionale(new GregorianCalendar(2018,0,12,16,8), new GregorianCalendar(2018,0,15,0,0),true);
		Promozione promo = new ScontoSemplice(50);
		campagna.add(zucchini, promo);
		
		try {
			campagna.setPuntiFedelta(1, 10);
		} catch (UnsupportedOperationException e) {
			System.out.println("La campagna non � riservata!");
		}
		
		CampagnaPromozionale campagna2 = new CampagnaPromozionale(new GregorianCalendar(2018,0,12,16,8), new GregorianCalendar(2018,0,15,0,0),false);
		Promozione promo2 = new ScontoSemplice(25);
		campagna2.add(zucchini, promo2);
		campagna2.add(fagioliBounduelle, promo2);
		
		System.out.println(campagna2.prezzo(zucchini, 4, new GregorianCalendar(), true));


	}

}
