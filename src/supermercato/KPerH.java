package supermercato;

public class KPerH implements Promozione{
	
	private int K,H;
	
	public KPerH(int K,int H){
		this.K = K;
		this.H = H;
	}
	
	public double prezzoScontato(int quantita,double prezzo){
		double price = 0;
		price += ((quantita/K)*H)*prezzo;
		double prova = (quantita % K) * prezzo;
		price += prova;

		return price;
	}

	public static void main(String[] args) {
		KPerH sconto = new KPerH(3,2);
		System.out.println(sconto.prezzoScontato(4, 1.3));
	}

}
