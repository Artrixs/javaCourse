/**
 * Rappresenta tutto un supermercato e le varie interazioni con esso possibili
 * @author Arturo Misino
 * @version 1.0
 */
package supermercato;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

public class Supermercato {
	//CAMPI
	private ArrayList<CampagnaPromozionale> campagne;
	private Map<ClienteFedele,Integer> clienti;
	
	//COSTRUTTORI
	/**
	 * Costruisce un oggetto di tipo supermercato.
	 */
	public Supermercato() {
		this.campagne = new ArrayList<CampagnaPromozionale>();
		this.clienti = new HashMap<ClienteFedele,Integer>();
	}
	
	//METODI
	/**
	 * Aggiunge una campagna promozionale al supermercato
	 * @param c CampagnaPromozionale da aggiungere.
	 */
	public void add(CampagnaPromozionale c) {
		campagne.add(c);
	}
	
	/**
	 * Chiude il conto relativo al Carrello c
	 * @param c Carello da considerare
	 * @return totale da pagare per la spesa nel carrello
	 */
	public double vaiAllaCassa(Carrello c) {
		double totale = 0;
		for(int i=0;i<c.size();i++) {
			Prodotto prod = c.get(i);
			int quantita = c.quantity(i);
			double prezzoMin = prod.getPrezzo() * quantita;
			for(CampagnaPromozionale campagna:campagne) {
				if(campagna.prezzo(prod, quantita,c.getData(), c.getFidelity())<prezzoMin)
					prezzoMin = campagna.prezzo(prod, quantita,c.getData(), c.getFidelity());
			}
			totale += prezzoMin;
		}
		
		return totale;
	}
	
	/**
	 * Aggiunge un cliente con carta fedelt�
	 * @param cliente Cliente con la carta feldet�
	 */
	public void addCliente(ClienteFedele cliente) {
		clienti.put(cliente, 0);
	}
	
	/**
	 * Calcola e aggiorna quanti punti un cliente fedele riceve con la spesa nel carrello
	 * @param c Carrello di cui calcolare i punti
	 * @param cliente ClienteFedele su cui eseguire l'operazione
	 */
	public void aggiornaPunti(Carrello c,ClienteFedele cliente) throws IllegalArgumentException {
		if(!clienti.containsKey(cliente))
			throw new IllegalArgumentException();
		else {
			int punti = clienti.get(cliente);
			for(int i=0;i<c.size();i++) {
				Prodotto prod = c.get(i);
				int quantita = c.quantity(i);
				double prezzoMin = prod.getPrezzo() * quantita;
				int puntiMax = 0;
				for(CampagnaPromozionale campagna:campagne) {
					if(campagna.prezzo(prod, quantita,c.getData(), c.getFidelity())<prezzoMin) {
						prezzoMin = campagna.prezzo(prod, quantita,c.getData(), c.getFidelity());
						puntiMax = campagna.punti(prod, quantita, c.getData(), c.getFidelity());
					}
				}
				punti += puntiMax;
			}
			clienti.replace(cliente,punti);
		}
	}
	
	/**
	 * Restituisce i punti che ha un cliente fedele
	 * @param cliente ClienteFedele di cui si vuole sapere il numero di punti
	 * @return il numero di punti del cliente
	 */
	public int getPunti(ClienteFedele cliente) {
		return clienti.get(cliente);
	}
	

	public static void main(String[] args) {
		//Genero prodotti e categorie merceologiche
		Prodotto fagioliBounduelle, zucchini, cetrioli;
		CategoriaMerceologica verdura,verduraFresca,verduraScatola;
		
		verdura = new CategoriaMerceologica("Verdura","verdure");	
		verduraFresca = new CategoriaMerceologica("Verdura Fresca","verdure",verdura);
		verduraScatola = new CategoriaMerceologica("Verdura in scatola","verdure",verdura);
		
		fagioliBounduelle = new Prodotto("Fagioli","Bonduelle",verduraScatola,"ABC67437",4);
		zucchini = new Prodotto("Zucchine","-",verduraFresca,"ABs67437",2);
		cetrioli = new Prodotto("Cetrioli","-", verduraFresca,"cdss45",1.5);
		
		
		// Campagne Promozionali
		CampagnaPromozionale campagna1 = new CampagnaPromozionale(new GregorianCalendar(2018,0,12,16,8), new GregorianCalendar(2018,0,15,0,0),true);
		Promozione promo = new ScontoSemplice(50);
		campagna1.add(zucchini, promo);
		
		try {
			campagna1.setPuntiFedelta(1, 1);
		} catch (UnsupportedOperationException e) {
			System.out.println("La campagna non � riservata!");
		}
		
		CampagnaPromozionale campagna2 = new CampagnaPromozionale(new GregorianCalendar(2018,0,12,16,8), new GregorianCalendar(2018,0,15,0,0),true);
		Promozione promo2 = new ScontoSemplice(25);
		campagna2.add(zucchini, promo2);
		campagna2.add(fagioliBounduelle, promo2);
		
		try {
			campagna2.setPuntiFedelta(1, 1);
		} catch (UnsupportedOperationException e) {
			System.out.println("La campagna non � riservata!");
		}
		
		//Creo il supermercato
		Supermercato supermercato = new Supermercato();
		supermercato.add(campagna1);
		supermercato.add(campagna2);
		
		//Creo un cliente fedele
		ClienteFedele cliente1 = new ClienteFedele("Arturo","Misino","25",new GregorianCalendar(1996,5,7,15,0));
		supermercato.addCliente(cliente1);
		
		System.out.println("I punti del cliente sono: "+supermercato.getPunti(cliente1));

		
		//Creo il mio carrello
		Carrello carrello = new Carrello(new GregorianCalendar(),true);
		carrello.add(fagioliBounduelle, 5);
		carrello.add(zucchini, 4);
		carrello.add(cetrioli, 2);
		supermercato.aggiornaPunti(carrello, cliente1);
		System.out.println(carrello.toString());
		
		//Vado alla cassa
		System.out.println(supermercato.vaiAllaCassa(carrello));
		System.out.println("I nuovi punti sono: "+supermercato.getPunti(cliente1));
	}

}
