package cap3;
import prog.io.ConsoleInputManager;
import prog.io.ConsoleOutputManager;
import prog.utili.Intero;

public class NumeroDivisio {

	public static void main(String[] args) {
		ConsoleInputManager in = new ConsoleInputManager();
		ConsoleOutputManager out = new ConsoleOutputManager();
		
		Intero num = new Intero(in.readInt("numero? "));
		
		out.print("La lunghezza della parola "+ num.toString()+" e' "+num.intValue()+",");
		if(num.intValue()/2 == num.toString().length())
			out.println(" uguale alla met� di "+num.intValue());
		else
			out.println("mentre la met� di "+num.intValue()+" e' "+num.intValue()/2);

	}

}
