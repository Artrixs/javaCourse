package cap3;

import prog.io.ConsoleInputManager;
import prog.io.ConsoleOutputManager;

public class ConfrontaStringhe {

	public static void main(String[] args) {
		ConsoleInputManager in = new ConsoleInputManager();
		ConsoleOutputManager out = new ConsoleOutputManager();
		
		String one = in.readLine("prima stringa? ");
		String two = in.readLine("seconda stringa? ");
		
		if(one.compareTo(two)==0) 
			out.println("le stringhe sono uguali");
		else {
			out.println("ordine alfabetico ");
			if(one.compareTo(two)<0) {
				out.println(one);
				out.println(two);}
			else {
				out.println(two);
				out.println(one);
			}
			
			if(one.length() == two.length())
				out.println("Hanno la stessa lunghezza");
			else {
				out.println("Ordine di lunghezza: ");
				if(one.length()>two.length()) {
					out.println(two);
					out.println(one);
				}else {
					out.println(one);
					out.println(two);
				}
			}
		}

	}

}
