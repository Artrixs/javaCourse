package cap3;
import prog.io.*;
import prog.utili.Frazione;


public class ConfrontoFrazioni {

	public static void main(String[] args) {
		ConsoleInputManager in = new ConsoleInputManager();
		ConsoleOutputManager out = new ConsoleOutputManager();
		
		int num = in.readInt("Inserisci il numeratore");
		int den = in.readInt("Inserisci il denomiantore");
		
		Frazione f1 = new Frazione(num,den);
		
		num = in.readInt("Inserisci il numeratore");
		den = in.readInt("Inserisci il denomiantore");
		
		Frazione f2 = new Frazione(num,den);
		if(f1.equals(f2))
			out.println("Le due frazioni sono ugauali");
		else if(f1.isMaggiore(f2))
			out.println("La frazione pi� grande � la prima " + f1.toString());
		else 
			out.println("La frazione pi� grande � la seconda " + f2.toString());

	}

}
