package cap3;

import prog.io.ConsoleInputManager;
import prog.io.ConsoleOutputManager;
import prog.utili.Frazione;

public class MediaSequenza {

	public static void main(String[] args) {
		ConsoleInputManager in = new ConsoleInputManager();
		ConsoleOutputManager out = new ConsoleOutputManager();

		Frazione somma = new Frazione(0);
		int conta = 0;
		boolean continua = in.readSiNo("Vuoi inserire una frazione? ");
		while(continua) {
			int num = in.readInt("Inserisci il numeratore: ");
			int den = in.readInt("Inserisci il denominatore: ");
			conta++;
			somma = somma.piu(new Frazione(num,den));
			continua = in.readSiNo("Vuoi inserire un'altra frazione? ");
			}

		if(conta == 0)
			out.println("Non sono state inserite frazioni.");
		else {
			out.println("La somma delle frazioni inserite e': "+somma.toString());
			Frazione media = somma.diviso(new Frazione(conta));
			out.println("La media delle frazioni inserite e': "+media.toString());
		}
			
	}

}
