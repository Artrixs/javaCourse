package cap3;

import prog.io.ConsoleInputManager;
import prog.io.ConsoleOutputManager;

public class CorniceNome {

	public static void main(String[] args) {
		ConsoleInputManager in = new ConsoleInputManager();
		ConsoleOutputManager out = new ConsoleOutputManager();
		
		String nome = in.readLine("Inserisci il tuo nome: ");
		String cognome = in.readLine("Inserisci il tuo cognome: ");
		
		int length;
		if (nome.length()>cognome.length()) {
			length = nome.length();
			
		}
		else
			length = cognome.length();
		
		String top = "";
		for(int i = 0;i<length+4;i++)
			top = top.concat("*");
		
		String middle = "*";
		for(int i=0;i<length+2;i++)
			middle = middle.concat(" ");
		middle = middle.concat("*");
		
		out.println(top);
		out.println(middle);
		out.println("* "+nome+" *");
		out.println(middle);
		out.println("* "+cognome+" *");
		out.println(middle);
		out.println(top);

	}

}
