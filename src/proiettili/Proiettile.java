package proiettili;

import geometria.Quadratica;
import geometria.Punto;
import java.math.*;

public class Proiettile {
	
	Quadratica traiettoria;
	
	//Costruttori
	public Proiettile(double theta,double v) {
		theta = Math.toRadians(theta);
		traiettoria = new Quadratica(-9.8/(2*v*v*Math.cos(theta)*Math.cos(theta)),Math.tan(theta));
	}
	
	//Metodi
	public String toString() {
		return "Proiettile con traiettoria: "+traiettoria.toString();
	}
	
	public boolean equals(Proiettile p) {
		return traiettoria.equals(p.traiettoria);
	}
	
	public Punto getMaximumHeightPoint() {
		return traiettoria.getVertex();
	}
	
	public Punto getLandingPoint() {
		Punto[] array =  traiettoria.getIntersectionWithXAxis();
		if(array.length == 1)
			return array[0];
		else
			return array[1];
	}
	
	public boolean isHitting(Punto p) {
		if(p.getY()>=0 && traiettoria.isIncluding(p))
			return true;
		else
			return false;
	}

	public static void main(String[] args) {
		Proiettile proj = new Proiettile(45,82);
		System.out.println(proj.getLandingPoint());

	}

}
