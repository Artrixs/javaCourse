package proiettili;

import geometria.Punto;

public class ProiettileEsplosivo extends Proiettile {
	
	double radius;
	
	public ProiettileEsplosivo(double theta, double v, double radius) {
		super(theta,v);
		this.radius = radius;
	}
	
	//Metodi
	public String toString() {
		return super.toString() + "e raggio di esplosione: "+radius;
	}
	
	public boolean equals(ProiettileEsplosivo p) {
		return traiettoria == p.traiettoria && radius == p.radius;
	}
	
	public boolean isHitting(Punto p) {
	if(super.isHitting(p)||getLandingPoint().getDistance(p)<=radius)
		return true;
	else
		return false;				
	}
	

	public static void main(String[] args) {
	ProiettileEsplosivo proj = new ProiettileEsplosivo(45,82,3);
	System.out.println(proj.toString());
	System.out.println(proj.isHitting(new Punto(690,0)));

	}

}
