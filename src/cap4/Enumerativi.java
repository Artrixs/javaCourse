package cap4;

import prog.io.*;
import prog.utili.MeseDellAnno;
import prog.utili.GiornoDellaSettimana;
import prog.utili.Data;

public class Enumerativi {

	public static void main(String[] args) {
		ConsoleOutputManager out = new ConsoleOutputManager();
		ConsoleInputManager in = new ConsoleInputManager();
		
		Data oggi = new Data();
		MeseDellAnno meseCorrente = oggi.getMeseDellAnno();
		int giorniNelMese = meseCorrente.numeroGiorni();
		int giorniAFine = giorniNelMese - oggi.getGiorno();
		
		switch(meseCorrente.successivo()) {
		case FEBBRAIO:
			giorniAFine += oggi.isInAnnoBisestile() ? 29 : 28;
		case MARZO:
			giorniAFine +=31;
		case APRILE:
			giorniAFine +=30;
		case MAGGIO:
			giorniAFine +=31;
		case GIUGNO:
			giorniAFine +=30;
		case LUGLIO:
			giorniAFine +=31;
		case AGOSTO:
			giorniAFine +=31;
		case SETTEMBRE:
			giorniAFine +=30;
		case OTTOBRE:
			giorniAFine +=31;
		case NOVEMBRE:
			giorniAFine +=30;
		case DICEMBRE:
			giorniAFine +=31;
		}
		
		out.println("Alla fine dell'anno mancano "+ giorniAFine+" giorni.");

	}
	
	private static String StampaData(Data data) {
		String print = "";
		print += data.getGiornoDellaSettimana().toString() + " " + data.getGiorno() + " ";
		print += data.getMeseDellAnno().toString() + " " + data.getAnno();
		return print;
	}
	

}
