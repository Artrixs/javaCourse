package cap5;

import prog.io.*;

public class CrivelloEratostene {

	public static void main(String[] args) {
		ConsoleOutputManager out = new ConsoleOutputManager();
		ConsoleInputManager in = new ConsoleInputManager();
		
		out.println("***Calcolo dei numeri primi***");
		int nMax = 0;
		//Controllo sull'input
		while((nMax = in.readInt("Massimo numero da considerare >")) <2)
			out.println("Deve essere un numero maggiore di 2.1");
		
		boolean[] primi = new boolean[nMax+1];
		for(int num = 2;num<nMax+1;num++) //Inizializzazione a tutti true
			primi[num] = true;
		
		//Esecuzioe di calcolo
		for(int numero=2;numero<nMax+1;numero++) {
			if(primi[numero]==true)// se � un primo cancello tutti i suoi multipli
				for(int mult = numero*2;mult<nMax+1;mult+=numero) 
						primi[mult] = false;
		}

		//Risultato
		out.println("I numeri primi fino a "+nMax+" sono:");
		for(int num = 2;num<nMax+1;num++)
			if(primi[num]==true)
				out.print(num+" ");
	}

}
