package geometria;

import java.util.Arrays;
import java.util.Scanner;

import people.Persona;

public class Punto implements Comparable<Punto>{
	
	private double x,y;

	
	//Constructors
	public Punto(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Punto(double x) {
		this.x = x;
		this.y = x;
	}
	
	public Punto() {
		this.x = 0;
		this.y = 0;
	}

	
	//Methods
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	
	public void setXY(double x,double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getRho(){
		double result = Math.sqrt(x*x+y*y);
		return result;
	}
	
	public double getTheta() {
		double result = Math.atan2(y,x);
		return result;
	}
	
	public String toString() {
		return "("+x+"," +y+")";
	}
	
	public boolean equals(Punto p) {
		if(this.x == p.x)
			if(this.y == p.y)
				return true;
		return false;						
	}
	
	public Punto puntoMedio(Punto p) {
		Punto result = new Punto();
		result.setX((this.x + p.x)/2);
		result.setY((this.y + p.y)/2);
		return result;
	}
	
	public double getDistance(Punto p) {
		double distX = x - p.x;
		double distY = y - p.y;
		return Math.sqrt(distX*distX + distY*distY);
	}
	
	public int compareTo(Punto p) {
		Punto orig = new Punto();
		if(this.getDistance(orig)<p.getDistance(orig))
			return -1;
		else if(this.getDistance(orig)==p.getDistance(orig))
			return 0;
		else
			return 1;
	}
	
	//PROVE
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int dim;
		System.out.println("Quanti punti vuoi vuoi inserire:");
		dim = in.nextInt();
		Punto[] array = new Punto[dim];
		for(int i = 0;i<dim;i++) {
			System.out.println("Inserisci x,y");
			String[] temp = in.next().split(",");
			array[i] = new Punto(Double.valueOf(temp[0]),Double.valueOf(temp[1]));
		}
		Arrays.sort(array);
		for(int i=0;i<dim;i++)
			System.out.println(array[i]);
		
		in.close();
		
	}
}
