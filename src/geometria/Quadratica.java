package geometria;

import java.util.Scanner;
import java.math.*;

public class Quadratica {
	//CAMPI
	double a,b,c;
	
	//COSTRUTTORI
	public Quadratica(double a,double b,double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public Quadratica(double a,double b){
		this.a = a;
		this.b = b;
		this.c = 0;
	}
	
	public Quadratica(double a){
		this.a = a;
		this.b = 0;
		this.c = 0;
	}
	
	//Metodi
	public String toString() {
		String result = a+"x^2 ";
		if(b>=0)
			result +="+";
		result +=b+"x ";
		if(c>=0)
			result +="+";
		result +=c;
		return result;
	}
	
	public boolean equals(Quadratica quad) {
		return a == quad.a && b==quad.b&&c==quad.c;
	}
	
	public Punto getIntersectionWithYAxis() {
		return new Punto(0,c);
	}
	
	public Punto[] getIntersectionWithXAxis() {
		int dim = 0;
		double delta = b*b-4*a*c;
		Punto[] array;
		if(delta>0) {
			array = new Punto[2];
			array[0] = new Punto((-b + Math.sqrt(delta))/(2*a),0);
			array[1] = new Punto((-b - Math.sqrt(delta))/(2*a),0);
			if(array[0].getX()>array[1].getX()) { //Riordina per ascisse crescenti
				Punto temp = array[0];
				array[0] = array[1];
				array[1] = temp;
			}
			return array;
		}else if(delta == 0) {
			array = new Punto[1];
			array[0] = new Punto(-b/(2*a),0);
			return array;
		}else
			return null;
	}
	
	public double getYof(double x) {
		return a*x*x + b*x+c;
	}
	
	public Punto getVertex() {
		return new Punto((-b/(2*a)),getYof((-b/(2*a))));
	}
	
	public boolean isAbove(Punto p) {
		return p.getY() < getYof(p.getX());
	}
	
	public boolean isBelow(Punto p) {
		return p.getY() > getYof(p.getX());
	}
	
	public boolean isIncluding(Punto p) {
		return p.getY() == getYof(p.getX()); 
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		Quadratica quad = new Quadratica(in.nextDouble(),in.nextDouble(),in.nextDouble());
		in.close();

	}
	

}
