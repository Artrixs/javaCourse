package geometria;


/**
 * Rappresenta una retta nel piano cartesiano, implementata come ax +by +c=0
 * @author Arturo Misino
 * #version 1.0
 */
public class Retta {
	
	double[] coeff;
	/**
	 * Costruisce la bisettrice I/III quadrante
	 */
	public Retta() {
		coeff = normalize(-1,1,0);
	}
	
	/**
	 * Costruisce la retta y = mx+q
	 * @param m coefficente angolare
	 * @param q termine noto
	 * @see #Retta()
	 */
	public Retta(double m,double q) {
		coeff = normalize(-m,1,-q);
	}
	
	/**
	 * Costruisce la retta x = c
	 * @param x retta verticale per (x,0)
	 */
	public Retta(double x) {
		coeff = normalize(1,0,-x);
	}
	
	/**
	 * Costruisce la retta passante per p e parallela a r
	 * @param r retta a cui � parallela
	 * @param p punto per cui passa
	 */
	public Retta(Retta r, Punto p) {
		  if(r.coeff[1]==0)
			  coeff = normalize(1,0,-p.getX());
		  else
			  coeff = normalize(r.coeff[0],1,-r.coeff[0]*p.getX()-p.getY());			  
	}
	
	/**
	 * Costruisce la retta perpendicolare a r passante per p
	 * @param p punto per cui passa
	 * @param r retta a cui � perpendicolare
	 */
	public Retta(Punto p, Retta r) {
		if(r.coeff[0]==0) 
			coeff = normalize(1,0,-p.getX());
		else if(r.coeff[1] == 0)
			coeff = normalize(0,1,-p.getY());
		else
			coeff = normalize(-1/(r.coeff[0]),1,(1/r.coeff[0])*p.getX()-p.getY());
	}
	
	//METODI
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		if(coeff[1]==0)
			return "x = " + (-coeff[2]==0?0:-coeff[2]);
		else if(coeff[0]==0)
			return "y = "+(-coeff[2]==0?0:-coeff[2]);
		else {
			String result = "y ="+(-coeff[0])+"x ";
			if(coeff[2]==0)
				return result;
			else if(-coeff[2]<0)
				return result+(-coeff[2]);
			else
				return result+"+"+(-coeff[2]);
		}
	}
	
	/**
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if(!(o instanceof Retta))
			return false;
		else {
			Retta r = (Retta) o;
			return this.coeff[0] == r.coeff[0] && this.coeff[1] == r.coeff[1] && this.coeff[2] == r.coeff[2];
			}
	}
	
	/**
	 * Calcola l'ascissa del punto x sulla retta
	 */
	public double y(double x) {
		return  -coeff[0]*x-coeff[2];
	}
	
	/**
	 * Verifica se � parallela alla retta r.
	 */
	public boolean isParallela(Retta r){
		return this.coeff[0] == r.coeff[0] && this.coeff[1] == r.coeff[1];
	}
	
	/**
	 * Verifica se � perpendicolare alla retta r
	 */
	public boolean isPerpendicolare(Retta r) {
		if(coeff[0] == 0)
			return  r.coeff[1] == 0;
		else if(coeff[1] == 0)
			return r.coeff[0] == 0;
		else 
			return r.coeff[0]/r.coeff[1] == -this.coeff[1]/this.coeff[0];
	}
	
	/**
	 * Verifica se passa per un determinato punto
	 */
	public boolean passaPer(Punto p) {
		if(coeff[1]==0)
			return p.getX() == -coeff[2];
		else
			return p.getY() == -coeff[0]*p.getX()-coeff[2];
	}
	
	/**
	 * Verifica se passa per l'array di punti
	 */
	public boolean passaPer(Punto[] p) {
		boolean verifi = true;
		for(int i=0;i<p.length;i++)
			if(!passaPer(p[i]))
				verifi = false;
		return verifi;
	}
	
	//METODI STATICI
	
	/**
	 * Funzione ausiiari per riportare i coefficenti in uno stato consistente per le uguaglianze
	 */
	private static double[] normalize(double a,double b, double c) {
		double[] result = new double[3];
		if(b!=0) {
			result[0] = Math.abs(a/b)==0? 0 :a/b;
			result[1] = 1;
			result[2] = Math.abs(c/b)==0? 0 :c/b;
		} else if(a!=0) {
			result[0] = 1;
			result[1] = 0;
			result[2] = Math.abs(c/a)==0? 0 :c/a;
		} else {
			result[0] = 0;
			result[1] = 0;
			result[2] = Math.abs(c);
		}
		return result;
	}

	public static void main(String[] args) {
		Retta r = new Retta(0,1);
		Punto[] p = {new Punto(2,1),new Punto(2,1), new Punto(3,2)};
		System.out.print(r.passaPer(p));
	}

}
