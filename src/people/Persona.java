package people;

import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.Arrays;
import java.util.Arrays.*;

public class Persona implements Comparable<Persona>{
	private int dataNascita;
	private String nome,cognome,luogoNascita;
	
	public Persona(String nome,String cognome,String luogoNascita, int dataNascita) {
		this.nome = nome;
		this.cognome = cognome;
		this.luogoNascita = luogoNascita;
		this.dataNascita = dataNascita;
	}

	public int getDataNascita() {
		return dataNascita;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public String getLuogoNascita() {
		return luogoNascita;
	}
	
	public String toString() {
		return nome + " " + cognome +" ("+luogoNascita+", "+dataNascita+")";
	}
	
	public boolean piuAnziana(Persona p) {
		if(this.dataNascita<p.dataNascita)
			return true;
		else
			return false;
	}

	public boolean stessaCitta(Persona p) {
		if(this.luogoNascita == p.luogoNascita)
			return true;
		else
			return false;
	}
	
	public int getEta() {
		return 2016-this.dataNascita;
	}
	
	public int compareTo(Persona pers) {
		if(this.getEta()<pers.getEta())
			return -1;
		else if(this.getEta() == pers.getEta())
			return 0;
		else
			return 1;
	}
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int dim;
		System.out.println("Quante persone vuoi inserire:");
		dim = in.nextInt();
		Persona[] array = new Persona[dim];
		for(int i = 0;i<dim;i++) {
			System.out.println("Inserisci nome,cognome,luogo di nascita,anno di nascita");
			String[] temp = in.next().split(",");
			array[i] = new Persona(temp[0],temp[1],temp[2],Integer.valueOf(temp[3]));
		}
		Arrays.sort(array);
		for(int i=0;i<dim;i++)
			System.out.println(array[i]);
		
		in.close();
		
	}

}
