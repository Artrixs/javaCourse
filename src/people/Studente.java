package people;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

public class Studente extends Persona{
	private String corsoLaurea;
	private int annoImmatricolazione;
	private ArrayList<String> esami;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Inserisci il tuo nome: ");
		String nome = sc.nextLine();
		System.out.print("Inserisci il tuo cognome: ");
		String cognome = sc.nextLine();
		System.out.print("Inserisci il tuo luogo di nascita: ");
		String luogoNascita = sc.nextLine();
		System.out.print("Inserisci il tuo anno di nascita: ");
		int annoNascita = sc.nextInt();
		System.out.print("Inserisci il tuo anno di immatricolazione: ");
		int annoImmatricolazione = sc.nextInt();
		System.out.print("Inserisci il tuo corso: ");
		String corsoLaurea = sc.nextLine();
		Studente studente = new Studente(nome,cognome,luogoNascita,annoNascita,corsoLaurea,annoImmatricolazione);
		
		System.out.println("Inserisci gli esami che hai sostenuto, stop se hai finito");
		String in = sc.nextLine();
		while(!in.equals("stop")) {
			studente.addCourse(in);
			System.out.println("Inserisci gli esami che hai sostenuto, stop se hai finito");
			in  = sc.nextLine();
		}
		
		System.out.println("Salve "+studente.toString() );
		System.out.println("Sei iscritto al corso di  "+studente.getCorsoLaurea()+ " dal "+studente.getAnnoImmatricolazione()
				+" e hai sostenuto "+studente.getNumberOfCourses()+" esami ("+studente.getAverageCoursesPerYear()+" esami all'anno)");
		System.out.println("Gli esami che hai sostenuto sono:");
		for(String object:studente.getEsami()) {
			System.out.println("- "+object);
		}

	}
	
	//Costruttori
	public Studente(String nome, String cognome, String luogoNascita, int annoNascita, String corsoDiLaurea, int annoImmatricolazione){
		super(nome,cognome,luogoNascita,annoNascita);
		this.corsoLaurea = corsoDiLaurea;
		this.esami = new ArrayList<String>();
		this.annoImmatricolazione = annoImmatricolazione;
	}
	
	public Studente(String nome, String cognome, String luogoNascita, int annoNascita, String corsoDiLaurea){
		super(nome,cognome,luogoNascita,annoNascita);
		this.corsoLaurea = corsoDiLaurea;
		this.esami = new ArrayList<String>();
		this.annoImmatricolazione = Calendar.getInstance().get(Calendar.YEAR);
	}
	
	//Metodi
	public void addCourse(String course) {
		esami.add(course);
	}
	
	public String getCorsoLaurea() {
		return corsoLaurea;
	}

	public int getAnnoImmatricolazione() {
		return annoImmatricolazione;
	}
	
	public ArrayList<String> getEsami(){
		return esami;
	}

	public Object[] getCourses(){
		return esami.toArray();
	}
	
	public int getNumberOfCourses() {
		return esami.size();
	}
	
	public boolean isCompletedCourse(String course) {
		return esami.contains(course);
	}
	
	public boolean isInCorso() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		if(annoImmatricolazione+3>currentYear)
			return true;
		else
			return false;
	}
	
	double getAverageCoursesPerYear() {
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		double trascorsi = currentYear - annoImmatricolazione +1;
		return esami.size()/trascorsi;
	}


}
